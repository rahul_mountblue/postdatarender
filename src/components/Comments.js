import React, { Component } from 'react'

 class Comments extends Component {
     constructor(props) {
         super(props)

     }
    render() {
        return (
            <div>
                {this.props.comments.length > 0 
                ?
                this.props.comments.map((comment)=>{
                    return <div className="comment-data">
                        <h1>{comment.name}</h1>
                        <p>{comment.body}</p>
                    </div>
                })
                :
                undefined
                }
            </div>
        )
    }
}

export default Comments;

