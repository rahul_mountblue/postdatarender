import React, { Component } from 'react'

 class Error extends Component {
    render() {
        return (
            <div className="error-page">
                <h1>!OOPS PAGE NOT FOUND </h1>
            </div>
        )
    }
}

export default Error;
