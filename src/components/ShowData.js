import React, { Component } from 'react'
import axios from 'axios';
import Posts from './Posts'

class ShowData extends Component {
    constructor(props) {
        super(props)
        this.state = {
           posts:[],
           comments:[],
           users:[],
           dataFetched:false,
        }
    }
        

    componentDidMount() {

       this.fetchData();
    }

    async fetchData () {
        try {
            let posts = await axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(posts => posts.data); 
    
            let users = await axios.get('https://jsonplaceholder.typicode.com/users')
            .then(users => users.data);
    
            let comments = await axios.get('https://jsonplaceholder.typicode.com/comments')
            .then(comments => comments.data);

            this.setState({
                posts:posts,
                users:users,
                comments:comments,
                dataFetched:true,
            })
        } catch (err) {
            console.log(err);
        }
        
    }


    
   shouldComponentUpdate(prevProps,prevState) {   
           if(prevState.posts !== this.state.posts && prevState.users !== this.state.users && prevState.comments !== this.state.comments) {
                   return true;
           }
           return false;
   }

    render() {
        const {posts,comments,users,dataFetched} = this.state;
        
        return (
            <div>
              {dataFetched ?
                <Posts posts={posts}  users={users} comments={comments}/>
                
                :
                <p> Loading...</p> 
            }                
            </div>
        )
    }
}


export default  ShowData;
