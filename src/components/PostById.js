import React, { Component } from 'react'
import Usericon from '../images/user.png'
import Comments from './Comments'
import CommentIcon from '../images/comments2.png'
import axios from 'axios'
import Error from './Error'
import {Link} from 'react-router-dom';

class PostById extends Component {
    constructor(props) {
        super(props)
        this.state = {
            postDataById: {},
            userData: {},
            postId:0,
            dataFetched: false,
            comArr: Array(101).fill(false)
        }
    }

    componentDidMount() {
        this.getData();

    }

    async getData() {
        try {
            let id = this.props.match.params.id;
            let postById = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
            let userDataById = await axios.get(`https://jsonplaceholder.typicode.com/users/${postById.data.userId}`)
            console.log(postById.data);
            this.setState({
                postDataById: postById.data,
                userData: userDataById.data,
                dataFetched: true,
                postId:id,
            })
        } catch (error) {
            console.log(error);
        }
        

    }


    // renderArr = (id) => {
    //     let comments = this.props.comments.filter(comment => comment.postId === Number(id));

    //     const tempArr = this.state.comArr
    //     tempArr[id] = !tempArr[id]
    //     this.setState({
    //         comArr: tempArr,
    //         comments: comments
    //     })
    // }


    render() {
        const { postDataById, userData, dataFetched,postId} = this.state;
        return (
            <div className="post-container">
                {dataFetched ?
                         <div className="container">
                            <div className="post-render-data">
                            <div className="username-container">
                                <img src={Usericon} alt="icon" />
                                <span>{this.state.userData.username}</span>
                                
                               </div>
                            <div className="allpost">
                             <Link to="/">Posts</Link>
                            </div>
                            </div> 
                            

                            

                            <div className="post-data-container">
                                <h1>{postDataById.title}</h1>
                                <p>{postDataById.body}</p>
                            </div>
                            {/* <div className="comment-container">
                                
                                <button value={post.id} onClick={() => { this.renderArr(post.id) }} className="comment"><span>
                                {this.state.comments.length} </span>
                                    <img src={CommentIcon} alt="comment" />
                                    </button>
                                {this.state.comArr[post.id] ? <Comments comments={this.state.comments} /> : undefined}
                            </div> */}
                        </div>
                    
                    :
                    postId > 100 && postId< 0 ? <Error/> : <p>Loading...</p> 
                      
                }

            </div>

        )
    }
}


export default PostById;
