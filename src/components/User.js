import React, { Component } from 'react'
import axios from 'axios';
import Usericon from '../images/user.png'
import Error from './Error';
import {Link} from 'react-router-dom';

class User extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userDataById: {},
            dataFetched: false,
            userID:0,
        }

    }
    componentDidMount() {
        this.findUserById();
    }

    async findUserById() {
        try {
            let id = this.props.match.params.id;

            let userDataById = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)

            let data = userDataById.data;
            console.log(userDataById.data);

            this.setState({
                userDataById: data,
                dataFetched: true,
                userID:id,

            })

        } catch (error) {
            console.log('data not fetched by id : ', error);
        }
    }

    render() {
        const { userDataById,userID } = this.state;
        console.log(userDataById);
        console.log(this.state);
        return (
            <div className="post-container">
                {this.state.dataFetched ?
                    <div className="userdata">

                        <div className="userbyid-container">
                            <div>
                            <img src={Usericon} alt="icon" />
                             <span className="username">
                                {userDataById.username}
                            </span>
                            </div>
                            <div>
                            <Link to="/">Posts</Link>
                            </div>
                            
                            
                                {/* <span className="name">
                                    {userDataById.name}
                                </span> */}
                            
                        </div>
                        <div className="about-user">
                            <div className="user-title">
                                <h2>Id</h2>
                                <h2>Email</h2>
                                <h2>Phoneno.</h2>
                                <h2>Company name</h2>
                                <h2>Website</h2>
                                <h2>Address</h2>
                            </div>
                            <div className="title-data">
                                <p>{userDataById.id}</p>
                                <p>{userDataById.email}</p>
                                <p>{userDataById.phone}</p>
                                <p>{userDataById.company.name}</p>
                                <p>{userDataById.website}</p>
                                <p><span>{userDataById.address.street}, </span> <span>{userDataById.address.city}, </span> <span>{userDataById.address.zipcode}</span></p>
                            </div>
                        </div>
                    </div>
                    :
                    userID > 10 && userID < 1 ? <Error /> : <p>Loading... </p>
                }

            </div>
        )
    }
}

export default User;
