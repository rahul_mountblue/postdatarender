import React, { Component } from 'react'
import Usericon from '../images/user.png'
import Comments from './Comments'
import CommentIcon from '../images/comments2.png'
import {Link} from 'react-router-dom';
class Posts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userData: {},
            comments: [],
            comArr: Array(101).fill(false)
        }
    }

    componentDidMount() {
        this.getUserData();
        let comments = this.props.comments.filter(comment => comment.postId === 1);
         this.setState({comments})
    }

    getUserData = () => {
        let data = this.props.users.reduce((userData, user) => {
            userData[user.id] = user.username;
            return userData;
        }, {})

        this.setState({ userData: data });
    }


    renderArr = (id) => {
        let comments = this.props.comments.filter(comment => comment.postId === Number(id));

        const tempArr = this.state.comArr
        tempArr[id] = !tempArr[id]
        this.setState({
            comArr: tempArr,
            comments: comments
        })
    }


    render() {
           console.log(this.state.comments);
        return (
            <div className="post-container">
                {this.props.posts.length > 0 ?
                    this.props.posts.map((post) => {
                        return <div className="container">

                            <div className="username-container">
                                <img src={Usericon} alt="icon" />
                                <Link to={`/user/${post.userId}`}>
                                    <span>{this.state.userData[post.userId]}</span>
                                </Link>
                                
                            </div>

                            <div className="post-data-container">
                                <Link to={`post/${post.id}`}>
                                <h1>{post.title}</h1>
                                </Link>
                                
                                <p>{post.body}</p>
                            </div>
                            <div className="comment-container">
                                
                                <button value={post.id} onClick={() => { this.renderArr(post.id) }} className="comment"><span>
                                {this.state.comments.length} </span>
                                    <img src={CommentIcon} alt="comment" />
                                    </button>
                                {this.state.comArr[post.id] ? <Comments comments={this.state.comments} /> : undefined}
                            </div>
                        </div>
                    })
                    :
                    undefined
                }

            </div>

        )
    }
}


export default Posts;
