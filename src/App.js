import React from 'react';
import './App.css';
import './css/reset.css'
import './css/style.css';
import ShowData  from './components/ShowData';
import User from './components/User';
import PostById from './components/PostById';
import Error from './components/Error'
import { Switch,Route } from "react-router-dom";

class App extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={ShowData} />

        <Route exact path="/user/:id" component={User} />
        <Route exact path="/post/:id" component={PostById} />
        <Route component={Error} />
    </Switch>
  )
  }
}

export default App;
